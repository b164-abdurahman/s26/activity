const http = require('http');

const port = 3000;

//creates a variable "server" that stores the output of the createServer method
const server = http.createServer((request, response) => {


	if(request.url == '/loginpage') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('you are in the loginpage')
	}
    else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not found')
	}
})


server.listen(port);
console.log(`Server now accessible at localhost:${port}.`);