3. The questions are as follows:
- What directive is used by Node.js in loading the modules it needs?
// Modules can load each other and use special directives export and import to interchange functionality, call functions of one module from another one:
- What Node.js module contains a method for server creation?
// The http module contains the function to create the server, which we will see later on. If you would like to learn more about modules in Node.
- What is the method of the http object responsible for creating a server using Node.js?
// Create a simple server using the . createServer() method and assign it to a const variable called server . Using the server object, make your newly created server listen on port 4001
- What method of the response object allows us to set status codes and content types?
// response.writehead
- Where will console.log() output its contents when run in Node.js?
// og() , which prints the string you pass to it to the console. If you pass an object, it will render it as a string. You can pass multiple variables to console.
- What property of the request object contains the address's endpoint?
// The req object represents the HTTP request and has properties for the request query string, parameters, body, HTTP headers, and so on.